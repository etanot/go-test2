To add new a package, add it in `packages.toml` file. And to add an exception,
add it in `exceptions.toml`. To run CI, commit your changes then push it.
